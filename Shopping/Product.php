<?php namespace abcSdk\Shopping;

class Product {

    protected $client;

    public function __construct(\abcSdk\Shopping\Shopping $client)
    {
        $this->client = $client;
    }

    public function getProducts(array $args)
    {
        $defaults = array(
            'q'                  => '',
            'brand_id'           => '',
            'category_id'        => '',
            'supplier_id'        => '',
            'product_status'     => '',
            'point'              => '',
            'recommended_status' => '',
            'promotion_status'   => '',
            'per_page'           => '',
            'orderby'            => 'created_at',
            'order'              => 'desc'
        );

        $args = array_merge($defaults, $args);

        $rs = $this->client->api('/products', $args, 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:getProducts] - something went wrong.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }
}