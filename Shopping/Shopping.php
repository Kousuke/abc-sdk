<?php namespace abcSdk\Shopping;

use Exception;
use abcSdk\Shopping\Order as Order;
use abcSdk\Shopping\Product as Product;
use abcSdk\Support\ClientAbstract as ClientAbstract;

class Shopping extends ClientAbstract {

    static $instance;

    public function __construct(array $config = array())
    {
        parent::__construct($config);


        static::$instance = array();
    }

    public function __call($name, $arguments)
    {
        if ( empty(static::$instance[$name]) )
        {
            if ( $name == 'order' )
            {
                static::$instance[$name] = new Order($this);
            }
            elseif ( $name == 'product' )
            {
                static::$instance[$name] = new Product($this);
            }
            else
            {
                throw new \Exception("Method does not exist.");
            }
        }

        return static::$instance[$name];
    }

}