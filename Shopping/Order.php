<?php namespace abcSdk\Shopping;

class Order {

    protected $client;

    public function __construct(\abcSdk\Shopping\Shopping $client)
    {
        $this->client = $client;
    }

    public function getOrders(array $args)
    {
        $defaults = array(
            'member_id'       => '',
            'per_page'        => '',
            'shipping_status' => '',
            'payment_status'  => 'success',
            'order_status'    => '',
            'orderby'         => 'created_at',
            'order'           => 'desc'
        );

        $args = array_merge($defaults, $args);

        $rs = $this->client->api('/orders', $args, 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:getOrders] - something went wrong.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function getOrder($orderId)
    {
        $rs = $this->client->api("/orders/{$orderId}", array(), 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:getOrder] - something went wrong.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }
}