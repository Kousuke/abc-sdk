<?php namespace abcSdk\Corporate;

use Exception;
use abcSdk\Support\ClientAbstract as ClientAbstract;


class Corporate extends ClientAbstract {

    protected $endpoint;
    protected $type;

    public function __construct(array $config = array())
    {
        $this->endpoint = $config['endpoint'];
        $this->type = $config['type'];
    }

    protected function getDefaultArgs()
    {
        return array(
            'type' => $this->type,
        );
    }

    /**
     * Get news from corporate site by API
     * @param void
     * @return array
     */
    public function getPrNews()
    {
        $news = $this->api('/posts');
        return json_decode($news['response']);
    }

    public function getPrNewsLimit($limit = 3)
    {
        $news = $this->getPrNews();

        if ( ! is_array($news) && ! is_object($news)) return [];

        if ($limit <= 0)
        {
            return $news;
        }

        $items = array_chunk($news, $limit);

        return $items[0];
    }

    /**
     * getSiteDetail - get corporate site detail
     * return array
     */
    public function getSiteUrl(){

        if(!$this->endpoint) return '';

        $url = parse_url($this->endpoint);

        return $url['scheme'].'://'.$url['host'];
    }

}
