<?php namespace abcSdk\Support;

abstract class ClientAbstract {

	protected $endpoint, $appId, $appSecret, $session;

	public function __construct(array $config = array())
	{
		$this->endpoint  = ( !empty($config['endpoint']) ) ? $config['endpoint'] : '' ;
		$this->appId     = ( !empty($config['application_id']) ) ? $config['application_id'] : '' ;
		$this->appSecret = ( !empty($config['secret']) ) ? $config['secret'] : '' ;
		$this->session   = ( !empty($config['session']) ) ? $config['session'] : '' ;
	}

	public function api($path, $args = array(), $method = 'GET')
	{
		$url = $this->getEndpoint($path);

		$defaultArgs = $this->getDefaultArgs();

		$args = array_merge($defaultArgs, $args);

		return $this->makeRequest($url, $args, $method);
	}

	public function setSession($session)
	{
		$this->session = $session;
	}

	protected function getDefaultArgs()
	{
		return array(
			'application_id' => $this->appId,
			'secret'         => $this->appSecret,
			'session'        => $this->session
		);
	}

	protected function getEndpoint($path)
	{
		return rtrim($this->endpoint, '/') . '/' . ltrim($path, '/');
	}

	protected function makeRequest($url, $data = array(), $method = 'GET', $curl_opts_extends = array())
	{
		$curl = curl_init();
		$data = http_build_query($data);

		$url = rtrim($url, '/');

		if (preg_match('/get/i', $method))
		{
			$url = strpos($url, '?') ? $url . '&' . $data : $url . '?' . $data;
			$data = null;
		}

		$curl_opts = array(
			CURLOPT_URL => $url,
			CURLOPT_CUSTOMREQUEST => strtoupper($method),
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => false,
			CURLOPT_SSL_VERIFYPEER => 2,
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_USERAGENT => 'gateway-sdk-0.1',
		);

		if (preg_match('/^https/', $url))
		{

			$curl_opts[CURLOPT_SSL_VERIFYHOST] = 0;
			$curl_opts[CURLOPT_SSL_VERIFYPEER] = 0;
		}

		// Override or extend curl options
		if (count($curl_opts_extends) > 0)
		{
			foreach ($curl_opts_extends as $key => $val)
			{
				$curl_opts[$key] = $val;
			}
		}

		curl_setopt_array($curl, $curl_opts);

		// Response returned.
		$response = curl_exec($curl);

		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		return array(
			'httpCode' => $httpCode,
			'response' => $response
		);
	}

}
