<?php namespace abcSdk\Service;

use Exception;
use abcSdk\Support\ClientAbstract as ClientAbstract;

class Service extends ClientAbstract {

    static $instance;

    public $applicationId;

    public function __construct(array $config = array())
    {
        parent::__construct($config);

        static::$instance = array();

        $this->applicationId = $this->appId;
    }

    public function __call($name, $arguments)
    {
        if ( empty(static::$instance[$name]) )
        {
            if ( $name == 'otp' )
            {
                static::$instance[$name] = new Otp($this);
            }
            elseif ( $name == 'sms' )
            {
                static::$instance[$name] = new Sms($this);
            }
            else
            {
                throw new \Exception("Method does not exist.");
            }
        }

        return static::$instance[$name];
    }

}
