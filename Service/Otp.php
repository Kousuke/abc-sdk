<?php namespace abcSdk\Service;

class Otp {

    protected $client;

    public function __construct(\abcSdk\Service\Service $client)
    {
        $this->client = $client;
    }

    public function generate($params = array())
    {
        $defaults = array(
            'app_id'     => $this->client->applicationId,
            'mobile'     => '',
            'text'       => '',
            'request_id' => ''
        );

        $data = array_merge($defaults, $params);

        $rs = $this->client->api("otp/generate", $data, 'POST');

        $response = json_decode($rs['response'], TRUE);

        if ( $rs['httpCode'] != 200 )
        {
            // s($rs); die();
            throw new \Exception("[Method:generate] - Something went wrong.");
        }

        /*
        return array(
            'expiration' => 0000000,
            'ref_id'     => "zzzzz"
        );
        */
        return $response;
    }

    public function verify($params = array())
    {
        $defaults = array(
            'app_id' => $this->client->applicationId,
            'ref_id' => '',
            'otp'    => ''
        );

        $data = array_merge($defaults, $params);

        $rs = $this->client->api("otp/verify", $data, 'POST');

        $response = json_decode($rs['response'], TRUE);

        if ( $rs['httpCode'] != 200 )
        {
            // s($rs); die();
            throw new \Exception("[Method:verify] - Something went wrong.");
        }

        /*
        return array(
            'valid'       => (0|1),
            'description' => "xxxxx"
        );
        */
        return $response;
    }

    public function findVerifiedOtp($params = array())
    {
        $defaults = array(
            'app_id' => $this->client->applicationId,
            'ref_id' => '',
            'mobile' => ''
        );

        $data = array_merge($defaults, $params);

        $rs = $this->client->api("otp/verified", $data, 'POST');

        $response = json_decode($rs['response'], TRUE);

        if ( $rs['httpCode'] != 200 )
        {
            // s($rs); die();
            throw new \Exception("[Method:findVerifiedOtp] - Something went wrong.");
        }

        return $response;
    }

    public function getRecentMessage($params = array())
    {
        $defaults = array(
            'app_id'        => $this->client->applicationId,
            'type'          => 'otp',
            'with_trash'    => 'no',
            'limit'         => 5
        );

        $data = array_merge($defaults, $params);

        $rs = $this->client->api("otp/recent", $data, 'GET');

        $response = json_decode($rs['response'], TRUE);

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:getRecentMessage] - Something went wrong.");
        }

        return $response;
    }

    public function listMessages($params = array())
    {
        $defaults = array(
            'app_id'    => $this->client->applicationId
        );

        $data = array_merge($defaults, $params);

        $rs = $this->client->api("otp/list-messages", $data, 'GET');

        $response = json_decode($rs['response'], TRUE);

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:listMessages] - Something went wrong.");
        }

        return $response;
    }

}