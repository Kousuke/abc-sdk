<?php namespace abcSdk\Service;

class Sms {

    protected $client;

    public function __construct(\abcSdk\Service\Service $client)
    {
        $this->client = $client;
    }

    public function send($params = array())
    {
        $defaults = array(
            'app_id'     => $this->client->applicationId,
            'mobile'     => '',
            'text'       => '',
            'request_id' => ''
        );

        $data = array_merge($defaults, $params);

        $rs = $this->client->api("sms/send", $data, 'POST');

        $response = json_decode($rs['response'], TRUE);

        if ( $rs['httpCode'] != 200 )
        {
            // s($rs); die();
            throw new \Exception("[Method:send] - Something went wrong.");
        }

        /*
        return array(
            'expiration' => 0000000,
            'ref_id'     => "zzzzz"
        );
        */
        return $response;
    }

}