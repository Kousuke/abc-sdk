<?php namespace abcSdk\Core;

use Exception;
use abcSdk\Core\Gateway as Gateway;
use abcSdk\Support\ClientAbstract as ClientAbstract;

class Core extends ClientAbstract {

    static $instance;

    public function __construct(array $config = array())
    {
        parent::__construct($config);

        static::$instance = array();
    }

    public function __call($name, $arguments)
    {
        if ( empty(static::$instance[$name]) )
        {
            if ( $name == 'gateway' )
            {
                static::$instance[$name] = new Gateway($this);
            }
            elseif ( $name == 'member' )
            {
                static::$instance[$name] = new Member($this);
            }
            elseif ( $name == 'point' )
            {
                static::$instance[$name] = new Point($this);
            }
            else
            {
                throw new \Exception("Method does not exist.");
            }
        }

        return static::$instance[$name];
    }

}
