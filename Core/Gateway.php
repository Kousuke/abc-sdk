<?php namespace abcSdk\Core;

class Gateway {

    protected $client;

    public function __construct(\abcSdk\Core\Core $client)
    {
        $this->client = $client;
    }

    public function requestToken(array $args = array(), $node = 'token')
    {
        $defaults = array(
            'invoice'        => '',
            'amount'         => '',
            'description'    => '',
            'foreground_url' => '',
            'background_url' => '',
            'ref1'           => '',
            'ref2'           => '',
            'ref3'           => '',
            'payer_email'    => ''
        );

        $args = array_merge($defaults, $args);

        $rs = $this->client->api('gateway/transactions', $args, 'POST');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:requestToken] - fields invoice, amount, foreground_url, background_url required.");
        }

        $response = json_decode($rs['response'], TRUE);

        if ( ! is_null($node) )
        {
            return $response[$node];
        }

        return $response;
    }

    public function getTransaction($token = '')
    {
        if ( empty($token) )
        {
            // throw new \Exception("[Method:getTransaction] - token not found.");
            return NULL;
        }

        $rs = $this->client->api("gateway/transactions/{$token}", array(), 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            // throw new \Exception("[Method:getTransaction] - token not found.");
            return NULL;
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function verify(array $args = array())
    {
        $defaults = array(
            'invoice'        => '',
            'amount'         => '',
            'token'          => ''
        );

        $args = array_merge($defaults, $args);

        $rs = $this->client->api('gateway/transactions/verify', $args, 'POST');

        if ( $rs['httpCode'] != 200 )
        {
            // throw new \Exception("[Method:verify] - transaction not found.");
            return NULL;
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

}