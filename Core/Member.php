<?php namespace abcSdk\Core;

class Member {

    protected $client;

    public function __construct(\abcSdk\Core\Core $client)
    {
        $this->client = $client;
    }

    public function findById($memberId = 0)
    {
        $rs = $this->client->api("members/{$memberId}", array(), 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:findById] - not found member.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function edit($memberId = 0, $data = array())
    {
        $rs = $this->client->api("members/{$memberId}", $data, 'POST');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:edit] - something went wrong.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function me()
    {
        return $this->findBySession($this->session);
    }

    public function findBySession($sessionId = 0)
    {
        $rs = $this->client->api("members/sessions/{$sessionId}", array(), 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:findById] - not found member.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function totalPoint($memberId = 0)
    {
        $rs = $this->client->api("members/{$memberId}/totalpoint", array(), 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:totalPoint] - not found member totalpoint.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response['total'];
    }

    // public function pointlogs($memberId = 0)
    // {
    //     $rs = $this->client->api("members/{$memberId}/pointlogs", array(), 'GET');

    //     if ( $rs['httpCode'] != 200 )
    //     {
    //         throw new \Exception("[Method:pointlogs] - not found member pointlogs.");
    //     }

    //     $response = json_decode($rs['response'], TRUE);

    //     return $response;
    // }

    public function getAddresses($memberId = 0)
    {
        $rs = $this->client->api("members/{$memberId}/addresses", array(), 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:getAddresses] - not found member addresses.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function getAddress($memberId = 0, $addressId = 0)
    {
        $rs = $this->client->api("members/{$memberId}/addresses/{$addressId}", array(), 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:getAddress] - not found member addresses.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function createAddress($memberId = 0, $data = array())
    {
        $defaults = array(
            'member_id'   => '',
            'title'       => '',
            'address'     => '',
            'postcode'    => '',
            'district_id' => '',
            'province_id' => '',
            'country_id'  => ''
        );

        $data = array_merge($defaults, $data);
        $data['member_id'] = $memberId;

        $rs = $this->client->api("members/{$memberId}/addresses", $data, 'POST');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:createAddress] - something went wrong.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function editAddress($memberId = 0, $addressId = 0, $data = array())
    {
        $defaults = array(
            'member_id'   => '',
            'title'       => '',
            'address'     => '',
            'postcode'    => '',
            'district_id' => '',
            'province_id' => '',
            'country_id'  => ''
        );

        $data = array_merge($defaults, $data);
        $data['member_id'] = $memberId;

        $rs = $this->client->api("members/{$memberId}/addresses/{$addressId}", $data, 'POST');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:editAddress] - something went wrong.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function deleteAddress($memberId = 0, $addressId = 0)
    {
        $rs = $this->client->api("members/{$memberId}/addresses/{$addressId}", array(), 'DELETE');

        // if ( $rs['httpCode'] != 200 )
        // {
        //     throw new \Exception("[Method:deleteAddress] - something went wrong.");
        // }

        return TRUE;
    }

    public function search($params = array())
    {
        $defaults = array(
            'q'          => '',
            'id'         => '',
            'card_no'    => '',
            'username'   => '',
            'first_name' => '',
            'last_name'  => '',
            'pid'        => '',
            'email'      => '',
            'mobile'     => '',
            'status'     => '',
            'page'       => '',
            'limit'      => '',
        );

        $data = array_merge($defaults, $params);

        $rs = $this->client->api("members", $data, 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:search] - something went wrong.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

}