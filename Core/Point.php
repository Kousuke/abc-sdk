<?php namespace abcSdk\Core;

class Point {

    protected $client;

    public function __construct(\abcSdk\Core\Core $client)
    {
        $this->client = $client;
    }
	
	public function refund(array $args = array())
	{
		$defaults = array(
			'member_id' => '',
			'refer_id' => '',
			'amount' => '',
			'description' => '',
			'ref1' => '',
			'ref2' => '',
			'ref3' => '',
		);

		$args = array_merge($defaults, $args);
		
		$rs = $this->client->api('points/refund', $args, 'POST');

        $response = json_decode($rs['response'], TRUE);

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:refund] - {$response['message']}, {$response['description']}");
        }

        return $response;
	}

	public function spend(array $args = array())
    {
        $defaults = array(
            // 'session'        => '',
            'member_id'      => '',
            'invoice'        => '',
            'amount'         => '',
            'description'    => '',
            'otp_ref'        => '',
            'ref1'           => '',
            'ref2'           => '',
            'ref3'           => '',
            'payer_email'    => ''
        );

        $args = array_merge($defaults, $args);

        $rs = $this->client->api('points/payment', $args, 'POST');

        $response = json_decode($rs['response'], TRUE);

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:spend] - {$response['message']}, {$response['description']}");
        }

        return $response;
    }

    public function transfer(array $args = array())
    {
        $defaults = array(
            // 'session'   => '',
            'from_member' => '',
            'to_member'   => '',
            'amount'      => ''
        );

        $args = array_merge($defaults, $args);

        $rs = $this->client->api('points/transfer', $args, 'POST');

        $response = json_decode($rs['response'], TRUE);

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:transfer] - {$response['message']} {$response['description']}");
        }

        return $response;
    }
}