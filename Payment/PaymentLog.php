<?php namespace abcSdk\Payment;

class PaymentLog {

    protected $client;

    public function __construct(\abcSdk\Payment\Payment $client)
    {
        $this->client = $client;
    }

    public function getPaymentLogs(array $args)
    {
        $defaults = array(
            'member_id'       => '',
            'payment_status'  => '',
            'page'            => '1',
            'limit'           => '10',
            'orderby'         => 'created_at',
            'order'           => 'desc'
        );

        $args = array_merge($defaults, $args);

        $rs = $this->client->api('/billing/logs', $args, 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:getPaymentLogs] - something went wrong.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }

    public function getPaymentLog($id)
    {
        $rs = $this->client->api("/billing/logs/{$id}", array(), 'GET');

        if ( $rs['httpCode'] != 200 )
        {
            throw new \Exception("[Method:getPaymentLog] - something went wrong.");
        }

        $response = json_decode($rs['response'], TRUE);

        return $response;
    }
}