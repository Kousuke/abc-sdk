<?php namespace abcSdk\Payment;

use Exception;
use abcSdk\Payment\PaymentLog as PaymentLog;
use abcSdk\Support\ClientAbstract as ClientAbstract;

class Payment extends ClientAbstract {

    static $instance;

    /**
     * [__construct description]
     * @param array $config [description]
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);

        static::$instance = array();
    }

    public function __call($name, $arguments)
    {
        if ( empty(static::$instance[$name]) )
        {
            if ( $name == 'log' )
            {
                static::$instance[$name] = new PaymentLog($this);
            }
            else
            {
                throw new \Exception("Method does not exist.");
            }
        }

        return static::$instance[$name];
    }

}
